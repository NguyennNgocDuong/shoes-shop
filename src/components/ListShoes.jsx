import React, { Component } from "react";
import ItemShoes from "./ItemShoes";

export default class ListShoes extends Component {
  renderListShoes = () => {
    return this.props.listShoes.map((item, index) => {
      return (
        <ItemShoes
          renderCart={this.props.renderCart}
          handleAddToCart={this.props.handleClick}
          key={index}
          shoes={item}
        />
      );
    });
  };

  render() {
    return (
      <div>
        <div className="row">{this.renderListShoes()}</div>
      </div>
    );
  }
}
