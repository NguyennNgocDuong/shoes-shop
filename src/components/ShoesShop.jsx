import React, { Component } from "react";
import ListShoes from "./ListShoes";
import { dataShoes } from "../data/dataShoes";
import Cart from "./Cart";

export default class ShoesShop extends Component {
  state = {
    listShoes: dataShoes,
    cart: [],
  };

  handleAddToCart = (shoes) => {
    let newCart = [...this.state.cart];
    let indexCart = newCart.findIndex((item) => {
      return item.id == shoes.id;
    });
    if (indexCart == -1) {
      let itemShoes = { ...shoes, soLuong: 1 };
      newCart.push(itemShoes);
    } else {
      newCart[indexCart].soLuong++;
    }
    this.setState({
      cart: newCart,
    });
  };

  render() {
    return (
      <div>
        <div className="container py-5">
          {this.state.cart.length > 0 ? (
            <Cart cart={this.state.cart} />
          ) : (
            <h2 className="text-danger">
              Bạn chưa có sản phẩm trong giỏ hàng!
            </h2>
          )}
          <ListShoes
            handleClick={this.handleAddToCart}
            listShoes={this.state.listShoes}
            renderCart={this.renderCart}
          />
        </div>
      </div>
    );
  }
}
