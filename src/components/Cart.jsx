import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <th style={{ verticalAlign: "middle" }} scope="row">
            {item.id}
          </th>
          <td style={{ verticalAlign: "middle" }}>{item.name}</td>
          <td style={{ verticalAlign: "middle" }}>${item.price}</td>
          <td style={{ verticalAlign: "middle" }}>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td style={{ verticalAlign: "middle" }}>{item.soLuong}</td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table table-hover table-dark">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Image</th>
            <th scope="col">Số lượng</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
