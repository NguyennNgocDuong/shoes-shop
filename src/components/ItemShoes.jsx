import React, { Component } from "react";
import styles from "./cardShoes.module.css";

export default class ItemShoes extends Component {
  render() {
    let { name, price, image } = this.props.shoes;

    return (
      <div className="col-3 my-3">
        <div
          style={{ height: "100%", cursor: "pointer" }}
          className={`card ${styles.card__item}`}
        >
          <img className="card-img-top" src={image} alt />
          <div
            className={`card-body ${styles.card__content}`}
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-around",
            }}
          >
            <h4 className="card-title">{name}</h4>
            <p className={`card-text ${styles.price}`}>${price}</p>
            <button
              onClick={() => this.props.handleAddToCart(this.props.shoes)}
              className="btn btn-dark"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}
